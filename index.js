var through = require("through2");
var dotted = require("dotted");
var fs = require("fs");
var path = require("path");
var Vinyl = require("vinyl");

module.exports = function(params)
{
    // Normalize the parameters.
    params = params || {};
    params.property = params.property || "access";
    params.files = params.files || {};

    // Set up and return the pipe.
    var cache = {};

    var pipe = through.obj(
        function(file, encoding, callback)
        {
            // See if we have the value requested. If we don't, then we stop
            // processing since there is nothing to do.
            var value = dotted.getNested(file, params.property);

            if (!value || !params.files[value])
            {
                return callback(null, file);
            }

            // Look to see if we have the value in the files section. If we
            // don't, then skip it.
            var inputFilename = path.join(process.cwd(), params.files[value]);

            if (!inputFilename)
            {
                return callback(null, file);
            }

            // Load the data into memory.
            var inputContents = cache[inputFilename];

            if (!inputContents)
            {
                inputContents = fs.readFileSync(inputFilename);
                cache[inputFilename] = inputContents;
            }

            // We have a file to insert into the stream. Make sure it is loaded.
            // Now that we have a file, we have to inject it
            // into the pipeline.
            var newFilename = path.join(file.data.relativePath, params.filename);
            var newFile = new Vinyl({
                cwd: "/",
                base: "/",
                path: newFilename,
                contents: inputContents
            });

            // Inject both files into the stream and then finish this item.
            this.push(file);
            this.push(newFile);

            callback();
        });

    return pipe;
}
